<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

?>

<?php $this->beginContent('@app/views/layouts/layout.php'); ?>

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => Yii::t('app', 'NAV_HOME'),
                'url' => ['/main/default/index'],
            ],
            [
                'label' => Yii::t('app', 'NAV_CONTACT'),
                'url' => ['/main/contact/index'],
            ],
            [
                'label' => Yii::t('app', 'NAV_SING_UP'),
                'url' => ['/user/default/signup'],
                'visible' => Yii::$app->user->isGuest,
            ],
            [
                'label' => Yii::t('app', 'NAV_LOGIN'),
                'url' => ['/user/default/login'],
                'visible' => Yii::$app->user->isGuest,
            ],
            [
                'label' => Yii::t('app', 'NAV_PROFILE'),
                'url' => ['/user/default/index'],
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => Yii::t('app', 'NAV_ADMIN'),
                'visible' => !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']],
                    ['label' => Yii::t('app', 'NAV_ADMIN_USERS'), 'url' => ['/admin/user/default/index']],
                ]
            ],
            [
                'label' => Yii::t('app', 'NAV_LOGOUT'),
                'url' => ['/user/default/logout'],
                'linkOptions' => ['data-method' => 'post'],
                'visible' => !Yii::$app->user->isGuest,
            ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

<?php $this->endContent(); ?>
