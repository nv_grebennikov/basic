<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 18.08.16
 * Time: 20:14
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\admin\rbac\Rbac as AdminRbac;
use app\modules\user\rbac\Rbac as UserRbac;
use app\modules\admin\rbac\ProfileRule;
use app\rbac\GroupRule;

class RbacController extends Controller
{
    /**
     * Generates roles
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAll();

        // module Admin
        $adminPanel = $auth->createPermission(AdminRbac::PERMISSION_ADMIN_PANEL);
        $adminPanel->description = 'Admin panel';
        $auth->add($adminPanel);

        // module User
        $userPanel = $auth->createPermission(UserRbac::PERMISSION_USER_PANEL);
        $userPanel->description = 'User panel';
        $auth->add($userPanel);

        $userPanelCreate = $auth->createPermission(UserRbac::PERMISSION_USER_CREATE_USER);
        $userPanelCreate->description = 'Create user';
        $auth->add($userPanelCreate);

        $userPanelUpdate = $auth->createPermission(UserRbac::PERMISSION_USER_UPDATE_USER);
        $userPanelUpdate->description = 'Update user';
        $auth->add($userPanelUpdate);

        $userPanelUpdateProfileRule = new ProfileRule();
        $auth->add($userPanelUpdateProfileRule);
        $userPanelUpdateProfile = $auth->createPermission(UserRbac::PERMISSION_USER_UPDATE_PROFILE);
        $userPanelUpdateProfile->description = 'Update user profile';
        $userPanelUpdateProfile->ruleName = $userPanelUpdateProfileRule->name;
        $auth->add($userPanelUpdateProfile);

        $userPanelView = $auth->createPermission(UserRbac::PERMISSION_USER_VIEW_USER);
        $userPanelView->description = 'View user';
        $auth->add($userPanelView);

        $userPanelDelete = $auth->createPermission(UserRbac::PERMISSION_USER_DELETE_USER);
        $userPanelDelete->description = 'Delete user';
        $auth->add($userPanelDelete);

        // create role
        $user = $auth->createRole('user');
        $user->description = 'User';
        $auth->add($user);
        $auth->addChild($user, $userPanelUpdateProfile);

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $auth->add($admin);
        $auth->addChild($admin, $user);
        $auth->addChild($admin, $adminPanel);
        $auth->addChild($admin, $userPanel);
        $auth->addChild($admin, $userPanelCreate);
        $auth->addChild($admin, $userPanelUpdate);
        $auth->addChild($admin, $userPanelView);
        $auth->addChild($admin, $userPanelDelete);


        $this->stdout('Done!' . PHP_EOL);
    }
}