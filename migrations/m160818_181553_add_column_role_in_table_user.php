<?php

use yii\db\Migration;

class m160818_181553_add_column_role_in_table_user extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function up()
    {
        $this->execute("ALTER TABLE {{%user}} ADD role ENUM('guest', 'user', 'admin') NOT NULL DEFAULT 'guest'");
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'role');
    }
}
