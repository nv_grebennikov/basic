<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'admin@example.com',
    'noReply' => 'admin@example.com',
];