<?php

return [
    'adminEmail' => '',
    'supportEmail' => '',
    'noReply' => '',
    'user.passwordResetTokenExpire' => 3600 * 24,
];
