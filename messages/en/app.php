<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 17:58
 */

return [
    'NAV_HOME' => 'Home',
    'NAV_CONTACT' => 'Contact',
    'NAV_SING_UP' => 'Sign Up',
    'NAV_LOGIN' => 'Login',
    'NAV_LOGOUT' => 'Logout',
    'NAV_PROFILE' => 'Profile',
    'NAV_ADMIN' => 'Admin panel',
    'NAV_ADMIN_USERS' => 'Users',

    'UPDATE' => 'Update',

    'CONFIRM_DELETE'=> 'Are you sure you want to delete this item?'
];