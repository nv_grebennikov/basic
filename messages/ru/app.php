<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 17:58
 */

return [
    'NAV_HOME' => 'Главная',
    'NAV_CONTACT' => 'Контакты',
    'NAV_SING_UP' => 'Регистрация',
    'NAV_LOGIN' => 'Вход',
    'NAV_LOGOUT' => 'Выход',
    'NAV_PROFILE' => 'Профиль',
    'NAV_ADMIN' => 'Админ панель',
    'NAV_ADMIN_USERS' => 'Пользователи',

    'UPDATE' => 'Update',

    'CONFIRM_DELETE'=> 'Вы уверены, что хотите удалить этот элемент?'


];