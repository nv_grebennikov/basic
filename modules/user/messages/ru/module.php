<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 17:58
 */

return [
    'PROFILE_INDEX_TITLE' => 'Просмотр профиля',
    'PROFILE_UPDATE_TITLE' => 'Редактирование профиля',
    'PROFILE_PASSWORD_CHANGE_TITLE' => 'Смена пароля',

    'USER_NEW_PASSWORD' => 'Новый пароль',
    'USER_REPEAT_PASSWORD' => 'Повторите пароль',
    'USER_CURRENT_PASSWORD' => 'Текущий пароль',
    'USER_CREATED' => 'Дата создания',
    'USER_UPDATED' => 'Дата редактирования',
    'USER_USERNAME' => 'Имя пользователя',
    'USER_EMAIL' => 'Email',
    'USER_STATUS' => 'Статус',
    'USER_DATE_FROM' => 'Дата от',
    'USER_DATE_TO' => 'Дата до',
    'USER_ROLE' => 'Роль',

    'ERROR_WRONG_CURRENT_PASSWORD' => 'Не верный пароль',

    'USERS_INDEX_TITLE' => 'Пользователи',
    'USERS_CREATE_TITLE' => 'Создать пользователя',
    'USERS_UPDATE_TITLE' => 'Редактирование пользователя: ',

    'BUTTON_CREATE_USER' => 'Добавить пользователя',
    'BUTTON_SAVE' => 'Сохранить',
    'BUTTON_UPDATE' => 'Редактировать',
    'BUTTON_DELETE' => 'Удалить',
    'BUTTON_PASSWORD_CHANGE' => 'Изменить пароль',
    'BUTTON_SEARCH' => 'Поиск',
    'BUTTON_RESET' => 'Сбросить',
    'BUTTON_CREATE' => 'Создать',

    'ROLE_GUEST' => 'Гость',
    'ROLE_USER' => 'Пользователь',
    'ROLE_ADMIN' => 'Администратор',
];