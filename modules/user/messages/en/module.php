<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 17:58
 */

return [
    'PROFILE_INDEX_TITLE' => 'View profile',
    'PROFILE_UPDATE_TITLE' => 'Update profile',
    'PROFILE_PASSWORD_CHANGE_TITLE' => 'Change password',

    'USER_NEW_PASSWORD' => 'New password',
    'USER_REPEAT_PASSWORD' => 'Repeat password',
    'USER_CURRENT_PASSWORD' => 'Current password',
    'USER_CREATED' => 'Created datetime',
    'USER_UPDATED' => 'Updated datetime',
    'USER_USERNAME' => 'Username',
    'USER_EMAIL' => 'Email',
    'USER_STATUS' => 'Status',
    'USER_DATE_FROM' => 'Date from',
    'USER_DATE_TO' => 'Date to',
    'USER_ROLE' => 'Role',

    'ERROR_WRONG_CURRENT_PASSWORD' => 'Without the right password',

    'USERS_INDEX_TITLE' => 'Users',
    'USERS_CREATE_TITLE' => 'Create User',
    'USERS_UPDATE_TITLE' => 'Update user: ',

    'BUTTON_CREATE_USER' => 'Create User',
    'BUTTON_SAVE' => 'Save',
    'BUTTON_UPDATE' => 'Update',
    'BUTTON_DELETE' => 'Delete',
    'BUTTON_PASSWORD_CHANGE' => 'Change password',
    'BUTTON_SEARCH' => 'Search',
    'BUTTON_RESET' => 'Reset',
    'BUTTON_CREATE' => 'Create',

    'ROLE_GUEST' => 'Guest',
    'ROLE_USER' => 'User',
    'ROLE_ADMIN' => 'Admin',
];