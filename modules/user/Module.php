<?php

namespace app\modules\user;

use yii\filters\AccessControl;
use Yii;
use app\modules\user\rbac\Rbac as UserRbac;
use app\modules\user\models\backend\User;

/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $defaultRole;

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\user\controllers';

    /**
     * @var int
     */
    public $passwordResetTokenExpire = 3600;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRbac::PERMISSION_USER_PANEL],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->defaultRole = User::ROLE_GUEST;

        // custom initialization code goes here
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/user/' . $category, $message, $params, $language);
    }
}
