<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 14:14
 */

namespace app\modules\user\forms\frontend;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    private $_user = false;
    private $_timeout;


    /**
     * PasswordResetRequestForm constructor.
     * @param integer $timeout
     * @param array $config
     */
    public function __construct($timeout, $config = [])
    {
        $this->_timeout = $timeout;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\modules\user\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        if ($this->_user === false) {
            $this->_user = User::findOne([
                'status' => User::STATUS_ACTIVE,
                'email' => $this->email,
            ]);
        }

        if (!$this->_user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($this->_user->password_reset_token, $this->_timeout)) {
            $this->_user->generatePasswordResetToken();
            if (!$this->_user->save()) {
                return false;
            }
        }
        return Yii::$app
            ->mailer
            ->compose('@app/modules/user/mails/passwordReset', ['user' => $this->_user])
            ->setFrom([Yii::$app->params['noReply'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}