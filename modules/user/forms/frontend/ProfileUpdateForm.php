<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 20:31
 */

namespace app\modules\user\forms\frontend;

use yii\base\Model;
use yii\db\ActiveQuery;
use app\modules\user\models\User;
use Yii;

class ProfileUpdateForm extends Model
{
    public $username;

    /**
     * @var User
     */
    private $_user;

    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    public function init()
    {
        $this->username = $this->_user->username;
        parent::init();
    }

    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'string', 'min' => 2, 'max' => 255],
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->username = $this->username;
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
        ];
    }
}