<?php

namespace app\modules\user\rbac;

/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 18.08.16
 * Time: 20:21
 */
class Rbac
{
    const PERMISSION_USER_PANEL = 'uaerPanel';
    const PERMISSION_USER_CREATE_USER = 'createUser';
    const PERMISSION_USER_UPDATE_USER = 'updateUser';
    const PERMISSION_USER_DELETE_USER = 'deleteUser';
    const PERMISSION_USER_VIEW_USER = 'viewUser';
    const PERMISSION_USER_UPDATE_PROFILE = 'userUpdateProfile';
}