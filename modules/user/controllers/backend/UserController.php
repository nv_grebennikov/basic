<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 16.08.16
 * Time: 21:20
 */

namespace app\modules\user\controllers\backend;

use app\modules\user\rbac\Rbac;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\user\forms\LoginForm;
use app\modules\user\forms\frontend\EmailConfirmForm;
use app\modules\user\forms\frontend\PasswordResetRequestForm;
use app\modules\user\models\PasswordResetForm;
use app\modules\user\forms\frontend\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}