<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 18.08.16
 * Time: 20:47
 */

namespace app\modules\admin\rbac;

use yii\rbac\Rule;

class ProfileRule extends Rule
{
    public $name = 'myProfile';

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['post']) ? $params['post']->id == $user : false;
    }
}