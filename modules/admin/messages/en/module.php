<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.16
 * Time: 17:58
 */

return [
    'NAV_ADMIN' => 'Admin panel',
    'NAV_ADMIN_USERS' => 'Users',

    'BUTTON_USERS' => 'Users',
];