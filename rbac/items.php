<?php
return [
    'permAdminPanel' => [
        'type' => 2,
        'description' => 'Admin panel',
    ],
    'uaerPanel' => [
        'type' => 2,
        'description' => 'User panel',
    ],
    'createUser' => [
        'type' => 2,
        'description' => 'Create user',
    ],
    'updateUser' => [
        'type' => 2,
        'description' => 'Update user',
    ],
    'userUpdateProfile' => [
        'type' => 2,
        'description' => 'Update user profile',
        'ruleName' => 'myProfile',
    ],
    'viewUser' => [
        'type' => 2,
        'description' => 'View user',
    ],
    'deleteUser' => [
        'type' => 2,
        'description' => 'Delete user',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
        'children' => [
            'userUpdateProfile',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'children' => [
            'user',
            'permAdminPanel',
            'uaerPanel',
            'createUser',
            'updateUser',
            'viewUser',
            'deleteUser',
        ],
    ],
];
